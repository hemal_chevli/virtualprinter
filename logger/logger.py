import time
import serial
import os
#############################
# Function Defs
# reconnect function called when device gets disconnected
def reconnect():
	#print "in reconnect"
	try:
		ser.close()
		ser.open()
		if ser.isOpen():
			print "Device Connected."
			txt = open(filename,'a') #reopen txt file
	except:
		print "*************"
		print "* Try Again *"
		print "*************"
		
		
def ensure_dir(f):#ensure dir present, create if not present
	d = os.path.dirname(f)
	if not os.path.exists(d):
		os.makedirs(d)
		#os.mkdir(d)
		print 'created'

def ensure_file(f):#ensure dir present, create if not present
	d = os.path.dirname(f)
	print d
	if not os.path.exists(d):
		txt = open(d,'a') 
		#os.mkdir(d)
		print 'created'
###############################

#Get today's date from system time'
timestr = time.strftime("%d-%m-%Y")
#set file name to system date
filename = timestr+'.txt'
#extract date from timestr
# get date only for comparision on date change
date = time.strftime("%d")

#Get current month and year for directory name
month_year = time.strftime("%m-%Y") +'/'
print month_year # change dir on this variable

#Set path where the dirs will be created
path = '/home/hemal/My_Stuff/gitlab/VirtualPrinter/logger/Master/'
dirname = path+month_year

print dirname

ensure_dir(dirname)

path_file = dirname+filename
print path_file
#New file created with today's date
#Opens text file with 'a' appends to file, w deletes all contents.
txt = open(path_file,'a') 
#diagnostics info
print "****************************************"
print "* Data will be saved in %s *" %path_file
print "****************************************"



#Harware connected
#for testing purpose
onpi = True

# configure the serial connections (the parameters differs on the device you are connecting to)
if onpi:
# start the serial port
	try:
		ser = serial.Serial(
#		port='/dev/ttyAMA0',# Pi
		port='/dev/ttyUSB0',#PC
		baudrate=9600,
		bytesize=8,
		parity='N',
		stopbits=1,
		timeout=None
		)
		ser.close()
		ser.open()
		ser.isOpen()
	except:
		#Port must be conncted when starting the program
		print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		print "! COULD NOT OPEN PORT, EXITING !"
		print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		exit(0)

while (1):
	try:
		#print str(onpi and ser.isOpen()) +" main"
		if onpi and ser.isOpen():
			read_line = ser.readline() #reads raw line
		else:
			read_line = raw_input()
			reconnect()
	
		linestr = read_line.rstrip() # removes extra newline
		print linestr
		clean_line= linestr
		try:
			clean_line.decode('ascii')
		except UnicodeDecodeError:
			print "Line dropped, contains junk values"
		else: 
			#Write only clean lines to txt file
			
			txt.write(linestr)
			txt.write('\n')
			#Empties the IO buffer 
			txt.flush()
			#Confirms writing to file
			os.fsync(txt)
		'''
		check if data has changed
		close current file and create new file and start logging 
		'''
		#Create new dir for new month
		newMonth_year = time.strftime("%m-%Y") +'/' #current month_year
		if newMonth_year != month_year: # month or year changed
			#create new dir
			month_year = newMonth_year
			#txt.close()
			dirname = path+month_year #new dirname
			ensure_dir(dirname) # create new dir 
			path_file = dirname+filename
			#update paths only 
		#create new file daily
		#ensure_dir(dirname) # create new dir 
		newDate = time.strftime("%d")
		if newDate != date:	#day changed
			#replace date with newDate
			date = newDate
			txt.close()
			#get new date
			
			timestr = time.strftime("%d-%m-%Y")
			path_file = dirname + timestr+'.txt'
			#ensure dir exists
			ensure_dir(path+month_year)
			txt = open(path_file,'a') 
			print "****************************************"
			print "* Data will be saved in %s *" %path_file
			print "****************************************"

	except serial.serialutil.SerialException:
	# Someone just unplugged the device, or other loss of communication.
		print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		print "! DEVICE DISCONNECTED, RECONNECT !" 
		print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		print " "
		print "*****************************************"
		print " Press ENTER after connecting to device *"
		print "*****************************************"
		#Close file with date and time when device was disconnected
		current_time = time.strftime("%d-%m-%Y    %H:%M:%S")
		txt.write('***************************************** Device disconnected at: '+current_time)
		#Empties the IO buffer 
		txt.flush()
		#CConfirms writing to file
		os.fsync(txt)
		#txt.close()
		#TODO: Play sound
		ser.close()
		continue
#not needed
	except ValueError:
		#error at float conversion
		print "File error"
		continue

	except IOError:
	# problem saving data
		print "Input/Output Error, exiting."
		break


'''TODO 
Play sound when disconnected
'''
