//Read from printer port
//Data lines
const int d0 = A4;
const int d1 = A3;
const int d2 = A2;
const int d3 = A1;
const int d4 = A0;
const int d5 = 13;
const int d6 = 12;
const int d7 = 11;

const int nStrobe = A5;
const int nAck = 10;
const int busy = 9;

const int ackWait = 2;//2 ms

char inByte;
char newinByte;

void setup(){
  //init serial
  Serial.begin(115200);
  //set io 

  pinMode(nStrobe,INPUT); 

  pinMode(d0,INPUT);
  pinMode(d1,INPUT);
  pinMode(d2,INPUT);
  pinMode(d3,INPUT);
  pinMode(d4,INPUT);
  pinMode(d5,INPUT);
  pinMode(d6,INPUT);
  pinMode(d7,INPUT);

  DDRC = B00000000;
  pinMode(nAck,OUTPUT);
  pinMode(busy,OUTPUT);

  //set printer select 13
  //set busy low
  //out of paper low
  //No error
  //connect strobe to interupt

  digitalWrite(busy,LOW);
  digitalWrite(nAck,LOW);

  //turn on LED to show boot compelete
  pinMode(2,OUTPUT);
  digitalWrite(2,LOW);
}
void loop(){
  inByte = read_8bit();
  newinByte = read_8bit();
  if (newinByte != inByte){
    Serial.print(inByte);
    delayMicroseconds(750);
  }
}

/*
New job ready (16 pin low)
 
 Monitor strob line to go low strobe flag
 as soon as strob line goes low set busy high
 
 read data byte when strob goes high(rising edge)
 togle nACK by giving a low pulse and then high(5us)
 store in RAM
 Wait for couple of seconds if the strobe is stable send data
 
 send byte to serial
 
 
 */

byte read_8bit(void){
  byte one_byte = 0;
  for (int i = 7; i >= 0; i--) { 
    byte bit = digitalRead(18-i);
    one_byte |= (bit << i);
  }
  return one_byte;
}


