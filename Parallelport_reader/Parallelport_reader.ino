//Read from printer port
//Data lines

//IO pins
const int nStrobe = 2;//int0
const int d0 = A4;
const int d1 = A3;
const int d2 = A2;
const int d3 = A1;
const int d4 = A0;
const int d5 = 13;
const int d6 = 12;
const int d7 = 11;
const int nAck = 10;
const int busy = 9;
const int paperout = 8;
const int select = 7;
const int error = 6;
const int resetpin = 5;
const int nSelectprinter = 4;
const int nLineFeed = A5;
const int led = 3;
//vars
const int ackWait = 2;//2 ms
char inByte;
volatile int data_present = 0;

void setup(){
  //init serial
  Serial.begin(115200);
  //set io 


  attachInterrupt(0, strobe, RISING);
  pinMode(d0,INPUT);
  pinMode(d1,INPUT);
  pinMode(d2,INPUT);
  pinMode(d3,INPUT);
  pinMode(d4,INPUT);
  pinMode(d5,INPUT);
  pinMode(d6,INPUT);
  pinMode(d7,INPUT);


  pinMode(nAck,OUTPUT);
  pinMode(busy,OUTPUT);
  pinMode(led,OUTPUT);
  digitalWrite(led,LOW);
  //set printer select 13
  //set busy low
  //out of paper low
  //No error
  //connect strobe to interupt

  digitalWrite(busy,LOW);
  digitalWrite(nAck,HIGH);
}
void loop(){

  if(data_present == 1){
    inByte = read_8bit();


    Serial.write(inByte);
    data_present = 0;
    

    
    //send ack here
  digitalWrite(nAck, LOW);       
  delayMicroseconds(ackWait);
  digitalWrite(nAck, HIGH);
    digitalWrite(busy,LOW);
  }
 

}

byte read_8bit(void){
  byte one_byte = 0;
  for (int i = 7; i >= 0; i--) { 
    byte bit = digitalRead(18-i);
    one_byte |= (bit << i);
  }
  return one_byte;
}

void strobe(){
  //set busy high
  digitalWrite(busy,HIGH);
  //set flag
  data_present = 1;
}

 //Send ack
  //def function later
  //toggle nack
  /*  digitalWrite(nAck,LOW);
   delay(ackWait);
   digitalWrite(nAck,HIGH);
   digitalWrite(busy,LOW);//Ready for more
   //}
   
  /*
   New job ready (16 pin low)
   
   Monitor strob line to go low strobe flag
   as soon as strob line goes low set busy high
   
   read data byte when strob goes high(rising edge)
   togle nACK by giving a low pulse and then high(5us)
   store in RAM
   Wait for couple of seconds if the strobe is stable send data
   
   send byte to serial
   
   
   */


